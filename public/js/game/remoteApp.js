﻿var isDebug = false;
var urlParams = {};

var ZebraGameRemote = function () {	
    var self = this;
    
    self.ZebrasInSpaceRemote = null;
    
    self.start = function () {
		self.InitUrlParameters();
		
		isDebug = Boolean(self.GetUrlParameter('isDebug'));
		
        self.ZebrasInSpaceRemote = zebraCorp.zebrasinspace.remote.initialize();
        
        // self.setupControllers();
		
		self.ZebrasInSpaceRemote.initRemote();
    };

	self.InitUrlParameters = function () {
        //set url parameters to global object
        (function () {
            var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.search.substring(1);
            while (e = r.exec(q))
                urlParams[d(e[1]).toLowerCase()] = d(e[2]);
        })();
    };
	
	self.GetUrlParameter = function (key) {
        return urlParams[key.toLowerCase()];
    };
};

var zebraGameRemote = new ZebraGameRemote();
var startZebraRemote = function () {
    zebraGameRemote.start();
};