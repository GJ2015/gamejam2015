// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.gameobject) == 'undefined') zebraCorp.zebrasinspace.display.gameobject = {};

zebraCorp.zebrasinspace.display.gameobject.extend = function (zebraGame) {
	
	zebraGame.createGameObject = function(objectId, health, moveSpeed, objectType, isDynamic){
		
		debug('zebrasinspace.display.player.createGameObject');
		
		var object = {
				ObjectId: objectId,
				Sprite: null,
				TextureName: '',
				IconName: null,
				IconSprite: null,
				ObjectType: objectType,
				Removed: false,
				
				VisibleIcon:false,
				
				IsMovingLeft: false,
				IsMovingRight: false,
				IsMovingUp: false,
				IsMovingDown: false,				
								
				Health: health,				
				ActionSuccessChance: 0,  // 0 to 10 where 0 any action on it is impossible (by design) and 10 is always success
				ImpactToShipsHealth: 0,  // 0 to 100 where 0 is no impact and 100 is maximum impact
				PointsAwaredOnActionSuccess: 0, // any amount
				HealthReducedOnActionFail: 0, // 0 to a lot
				HealthAdddedOnActionSuccess: 0, // 0 to a lot
				ChargeAdddedOnActionSuccess: 0, // 0 to 100
				
				IsDynamicThreath: isDynamic,
				
				MoveSpeed: moveSpeed				
		};
		
		object.loadSprite = function(){			
			debug('zebrasinspace.display.gameobject.loadSprite for ' + object.ObjectId);
			
			var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + object.TextureName);
			object.Sprite = new PIXI.Sprite(texture);		
			
			object.Sprite.height = 35;
			object.Sprite.width = 35;			

			object.Sprite.anchor.x = 0.5;
			object.Sprite.anchor.y = 0.5;	
			
			object.loadIcon();
		};
		
		
		object.loadIcon = function() {
			debug('zebrasinspace.display.gameobject.loadIcon for ' + object.ObjectId);
			
			if (object.IconName != null){				
				
				var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + object.IconName);
				object.IconSprite = new PIXI.Sprite(texture);		
				
				object.IconSprite.height = 35;
				object.IconSprite.width = 35;			

				object.IconSprite.anchor.x = 0.5;
				object.IconSprite.anchor.y = 0.5;	
			}			
		}
		
		object.setPosition = function(point)
		{
			object.Sprite.position.x = point.x;
			object.Sprite.position.y = point.y;	
			
			if (object.IconSprite != null){			
				object.IconSprite.position.x = point.x;
				object.IconSprite.position.y = point.y - 30;
			}
		};	
		
		object.update = function(gameObjects) {		
			object.doMovement(); //basicly no movement as all objects has MoveSpeed = 0
		
			if (object.ObjectType === 'fire01') {	
				//increase size of fire every 20 seconds			
				if (zebraGame.Ticks % (zebraGame.TicksPerSecond * 20) === 0) {					
					object.Sprite.width = object.Sprite.width * 1.25;
					object.Sprite.height = object.Sprite.height * 1.25;					
					
					zebraGame.impactShipsHealth(object.ImpactToShipsHealth)
					
				}				
				if (object.Sprite.width > 70) {
					
					zebraGame.stopLoopedAudio(object.loopedAudio);
					object.loopedAudio = "burning_2";
					zebraGame.playLoopedAudio(object.loopedAudio);					
				}				
				if (object.Sprite.width > 120) {
					zebraGame.stopLoopedAudio(object.loopedAudio);
					object.loopedAudio = "burning_3";
					zebraGame.playLoopedAudio(object.loopedAudio);
				}				
			}
			
			if (object.VisibleIcon) {
				if (zebraGame.Ticks % (zebraGame.TicksPerSecond) === 0) {	
					debug('zebrasinspace.display.gameObject.VisibleIcon 1 second');
					
					if (object.IconSprite.height < 50)
					{
						object.IconSprite.height =  object.IconSprite.height + 5;
						object.IconSprite.width =  object.IconSprite.width + 5;
					}	
					else 
					{
						object.IconSprite.height =  object.IconSprite.height - 5;
						object.IconSprite.width =  object.IconSprite.width - 5;
					}				
				}
			}
			
			
		};			
		
		object.doMovement = function()
		{
			if (object.IsMovingLeft){
				object.Sprite.position.x -= object.MoveSpeed;				
			}			
			if(object.IsMovingRight){
				object.Sprite.position.x += object.MoveSpeed;
			}
			if(object.IsMovingUp){
				object.Sprite.position.y -= object.MoveSpeed;
			}
			if(object.IsMovingDown){
				object.Sprite.position.y += object.MoveSpeed;
			}				
		};
		
		object.applyGoodActionFromPlayer = function(playerObject) {
			debug('good action applied on object with id: ' + object.ObjectId + ' from playerId: ' + playerObject.PlayerId);
			
			if (playerObject.BatteryCharge > 0) {
				var success = object.checkExecuteActionSuccess();
				
				if (success) {
					playerObject.changeScore(object.PointsAwaredOnActionSuccess);
				} else {
					zebraGame.sendMessage(playerObject.PlayerId, 'actionFailed');
				}
				
				//execute the action based on success
				object.executeGoodAction(success, playerObject);
				
				if(object.ObjectType !== 'chargstation01') {
					playerObject.changeBatteryCharge(-playerObject.BatteryActionUsage);
				}				
			}
		};
		
		object.applyBadActionFromPlayer = function(playerObject) {
			debug('bad action applied on object with id: ' + object.ObjectId + ' from playerId: ' + playerObject.PlayerId);
			
			if (playerObject.BatteryCharge > 0) {
				var success = object.checkExecuteActionSuccess();
				
				if (success && playerObject.IsInfected) {
					playerObject.changeScore(object.PointsAwaredOnActionSuccess);
				} else if (success && !playerObject.IsInfected) {
					playerObject.changeScore(-object.PointsAwaredOnActionSuccess);
				} else if (!success) {
					zebraGame.sendMessage(playerObject.PlayerId, 'actionFailed');
				}
				
				//execute the action based on success
				object.executeBadAction(success, playerObject);
				
				playerObject.changeBatteryCharge(-playerObject.BatteryActionUsage);
			}
		};

		object.checkExecuteActionSuccess = function() {
			var success = object.ActionSuccessChance === 10;
			
			if (!success && object.ActionSuccessChance !== 0) {
				//calculate
				success = zebraGame.RollDie(1,10) <= object.ActionSuccessChance;
				
				// hide icon
			}
			
			return success;
		};
		
		object.setObjectData = function(objectType) {
			if (objectType && objectType !== object.ObjectType) {
				object.ObjectType = objectType
			}
			
			if (object.ObjectType === 'fire01') {
				object.TextureName = "fire01.png";
				object.IconName = "fireIcon.png";
				object.ActionSuccessChance = 4;
				object.ImpactToShipsHealth = 10;
				object.PointsAwaredOnActionSuccess = 100;
				object.HealthReducedOnActionFail = 10;
				object.HealthAdddedOnActionSuccess = 0;
				object.VisibleIcon = true;
				object.loopedAudio = "burning_0";
				zebraGame.playLoopedAudio(object.loopedAudio);
				
				object.executeGoodAction = function(success, playerObject) {
					if (success) {
						object.Removed = true;						
						zebraGame.playLoopedAudio(object.loopedAudio);					
						
					} else {
						playerObject.changeHealth(-object.HealthReducedOnActionFail);
					}					
				};
				
				object.executeBadAction = function(success, playerObject) {
					if(success) {
						object.Sprite.width = object.Sprite.width * 1.5;
						object.Sprite.height = object.Sprite.height * 1.5;
					} else {	
						playerObject.changeHealth(-object.HealthReducedOnActionFail);
					}
				};
			}		
			
			if (object.ObjectType === 'oildrum01') {
				object.TextureName = "oildrum01.png";
				object.IconName = "oilspillIcon.png";
				object.ActionSuccessChance = 3;
				object.ImpactToShipsHealth = 0;
				object.PointsAwaredOnActionSuccess = 10;
				object.HealthReducedOnActionFail = 0;
				object.HealthAdddedOnActionSuccess = 0;
				object.VisibleIcon = false;
				
				object.executeGoodAction = function(success, playerObject) {
					if(success) {
						if (object.ImpactToShipsHealth > 0) {
							zebraGame.ReduceThreatCounter();
						}
						
						object.ImpactToShipsHealth = 0;
						object.ObjectType = "oildrum01";
						
						//switch image to oildrum01.png	
						var newTexture = PIXI.Texture.fromImage(zebraGame.gfxFolder + "oildrum01.png");
						object.Sprite.setTexture(newTexture);
						object.VisibleIcon = false;
						zebraGame.Scene.removeChild(object.IconSprite);
					}
				};
				
				object.executeBadAction = function(success, playerObject) {
					if(success) {						
						object.ImpactToShipsHealth = 5;
						
						object.ObjectType = "leakingoildrum01";
						
						zebraGame.NumberOfActiveThreaths++;
						
						//switch image to leakingoildrum01.png
						var newTexture = PIXI.Texture.fromImage(zebraGame.gfxFolder + "leakingoildrum01.png");
						object.Sprite.setTexture(newTexture);
						
						object.VisibleIcon = true;
						zebraGame.Scene.addChild(object.IconSprite);
						
						zebraGame.playAudio("oilspill_1");
						zebraGame.playAudio("oilleak");
					}
				};
			}
			
			if (object.ObjectType === 'chargstation01') {
				object.TextureName = "chargstation01.png";
				object.ActionSuccessChance = 100;
				object.ImpactToShipsHealth = 0;
				object.PointsAwaredOnActionSuccess = 0;
				object.HealthReducedOnActionFail = 0;
				object.HealthAdddedOnActionSuccess = 0;
				object.ChargeAdddedOnActionSuccess = 100;
				
				object.VisibleIcon = false;
				
				object.executeGoodAction = function(success, playerObject) {
					if(success) {
						playerObject.changeBatteryCharge(object.ChargeAdddedOnActionSuccess, true);
					}
				};
				
				object.executeBadAction = function(success, playerObject) {
					//nothing
				};
			}
			
			if (object.ObjectType === 'faultywire01') {
				object.TextureName = "faultywire01.png";
				object.IconName = "wiresIcon.png";
				object.ActionSuccessChance = 6;
				object.ImpactToShipsHealth = 5;
				object.PointsAwaredOnActionSuccess = 30;
				object.HealthReducedOnActionFail = 0;
				object.HealthAdddedOnActionSuccess = 0;
				
				object.VisibleIcon = false;
				
				object.executeGoodAction = function(success, playerObject) {
					if (success) {
						object.Removed = true;	
					} else {
						//add fire??
					}
				};
				
				object.executeBadAction = function(success, playerObject) {
					if (success) {
						//add fire
						zebraGame.addFireAtPosition(object.Sprite.position);
					}
				};
			}
		};

		object.setObjectData();
		
		return object;		
	};

    zebraGame.impactShipsHealth = function(impact){

        var previousIntegrity = zebraGame.CurrentShip.Integrity;

        zebraGame.CurrentShip.Integrity -= impact;

        // sound logic..... :|
        var number1 = null;
        var number2 = null;

        if (zebraGame.CurrentShip.Integrity < 20 || zebraGame.CurrentShip.Integrity == 100 || zebraGame.CurrentShip.Integrity % 10 == 0) {
            number1 = zebraGame.CurrentShip.Integrity + "";
        }
        else {
            var dec = parseInt(zebraGame.CurrentShip.Integrity / 10, 10);
            var pot = dec * 10;
            var last = zebraGame.CurrentShip.Integrity - pot;

            number1 = pot + "";
            number2 = last + "";
        }

        if ( (previousIntegrity >= 50)  && (zebraGame.CurrentShip.Integrity < 50 ) ) {
            if (number2 != null) {
                zebraGame.playAudioSequence("warning", "iminentdanger", "hullintegrity", number1, number2);
            }
            else{
                zebraGame.playAudioSequence("warning", "iminentdanger", "hullintegrity", number1);
            }
        }

        else if (zebraGame.CurrentShip.Destroyed){
            zebraGame.playAudioSequence("stupiddroids", "explosion_1");
        }

        else {

            if (number2 != null) {
                zebraGame.playAudioSequence("hullintegrity", number1, number2);
            }
            else{
                zebraGame.playAudioSequence("hullintegrity", number1);
            }
        }
    };

    zebraGame.explodeShip = function() {
        var loader = new PIXI.AssetLoader(["gfx/ExplosionSpriteSheet.json"]);
        var explosionTextures = [];
        loader.onAssetLoaded(function() {
            for (var i=0; i<26; i++) {
                var texture = new PIXI.Texture.fromFrame("Explosion_Sequence_A " + i+1 + ".png");
                explosionTextures.push(texture);
            }
            zebraGame.ExplosionAnimation = new PIXI.MovieClip(explosionTextures);
            zebraGame.ExplosionAnimation.x = 100;
            zebraGame.ExplosionAnimation.y = 100;
            zebraGame.ExplosionAnimation.anchor.x = 0.5;
            zebraGame.ExplosionAnimation.anchor.y = 0.5;
            zebraGame.ExplosionAnimation.scale = 10;
            zebraGame.ExplosionAnimation.gotoAndPlay();
            zebraGame.Root.addChild(zebraGame.ExplosionAnimation);
            requestAnimFrame(zebraGame.animate);
            zebraGame.Renderer.render(zebraGame.Stage);
        });
        loader.load();

    };

	zebraGame.gameOver = function() {
        debug("GAME OVER");
        zebraGame.explodeShip();
        // Empty screen
        zebraGame.emptyScreen();
		
		//game over screen
		var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + "EndScreen.png");
		var sprite = new PIXI.Sprite(texture);
		zebraGame.Scene.addChild(sprite);
		
        // High scores
        var players = zebraGame.Players;
        players.sort(function(a, b) {
            return parseInt(b.Score) - parseInt(a.Score);
        });
        var score = '';
        for (var i in players) {
            var index = parseInt(i) + 1;
            score += index + ". " + players[i].Username + "    " + players[i].Score + " points\n";
        }
        zebraGame.HighScoreList = new PIXI.Text(score,{font:"30px Arial", fill:"black"});
        zebraGame.HighScoreList.x = 650;
        zebraGame.HighScoreList.y = 665;
        zebraGame.Root.addChild(zebraGame.HighScoreList);
		
		zebraGame.Hud.removeChild(zebraGame.ShipIntegrityText);
		zebraGame.Hud.removeChild(zebraGame.TimeLeftText);

        for(var i = 0, l = zebraGame.Players.length; i < l; i++) {
            zebraGame.sendMessage(zebraGame.Players[i].PlayerId, 'gameover');
        }

    };	
};