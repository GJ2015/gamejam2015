﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.controllers) == 'undefined') zebraCorp.zebrasinspace.display.controllers = {};

zebraCorp.zebrasinspace.display.controllers.extend = function (zebraGame) {
    debug('zebrasinspace.controllers initializing');
	/*
	backspace 	8
	tab 	9
	enter 	13
	shift 	16
	ctrl 	17
	alt 	18
	pause/break 	19
	caps lock 	20
	escape 	27
	page up 	33
	page down 	34
	end 	35
	home 	36
	left arrow 	37
	up arrow 	38
	right arrow 	39
	down arrow 	40
	insert 	45
	delete 	46
	0 	48
	1 	49
	2 	50
	3 	51
	4 	52
	5 	53
	6 	54
	7 	55
	8 	56
	9 	57 	
	*/
	
    zebraGame.executeKeyDown = function (keyId) {		
        switch (keyId) {			
        	// q
        	case 81:
        		//debug('q down');  
				zebraGame.parseMessage("0:executeGoodAction");
        		break;
			// w
        	case 87:
        		//debug('w down'); 
				zebraGame.parseMessage("0:executeBadAction");
        		break;
			//left arrow
            case 37:   
				//debug('left arrow down');
				zebraGame.parseMessage("0:moveLeft");
				break;
			//up arrow
			case 38:  
				//debug('up arrow down');	
				zebraGame.parseMessage("0:moveUp");				
				break;
			//right arrow
			case 39:   
				//debug('right arrow down');
				zebraGame.parseMessage("0:moveRight");
				break;
			//down arrow
			case 40: 
				//debug('down arrow down');
				zebraGame.parseMessage("0:moveDown");
				break;
        }
    };

    zebraGame.executeKeyUp = function (keyId) {
        switch (keyId) {
			//left arrow
	        case 37:   
				//debug('left arrow up');
				zebraGame.parseMessage("0:stopLeft");
				break;
			//up arrow
			case 38:  
				//debug('up arrow up');	
				zebraGame.parseMessage("0:stopUp");				
				break;
			//right arrow
			case 39:   
				//debug('right arrow up');
				zebraGame.parseMessage("0:stopRight");
				break;
			//down arrow
			case 40: 
				//debug('down arrow up');
				zebraGame.parseMessage("0:stopDown");
				break;
        }
    };       
    
    zebraGame.doPlayerMovementLeft = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingLeft = true;
		}
	};
	
	zebraGame.doPlayerMovementRight = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingRight = true;
		}
	};
	
	zebraGame.doPlayerMovementUp = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingUp = true;
		}
	};
	
	zebraGame.doPlayerMovementDown = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingDown = true;
		}
	};
	
	zebraGame.stopPlayerMovementLeft = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingLeft = false;
		}
	};	
	
	zebraGame.stopPlayerMovementRight = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingRight = false;
		}
	};	
	
	zebraGame.stopPlayerMovementUp = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingUp = false;
		}
	};	
	
	zebraGame.stopPlayerMovementDown = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsMovingDown = false;
		}
	};	

	zebraGame.doPlayerGoodAction = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsDoingGoodAction = true;
		}
	};

	zebraGame.doPlayerBadAction = function(playerId) {
		var player = zebraGame.getPlayer(playerId);
		if (player) {
			player.IsDoingBadAction = true;
		}
	};
    
    
};