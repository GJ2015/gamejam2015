// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.hud) == 'undefined') zebraCorp.zebrasinspace.display.hud = {};

zebraCorp.zebrasinspace.display.hud.extend = function(zebraGame) {
	
	
	zebraGame.initHud = function() {
		zebraGame.GameIdText = new PIXI.Text("Wait",{font:"30px Arial", fill:"red"});
		zebraGame.GameIdText.x = 1500;
		zebraGame.GameIdText.y = 0;
		zebraGame.Hud.addChild(zebraGame.GameIdText);
	};
	
	zebraGame.setGameId = function(id) {
		zebraGame.GameIdText.setText("Game id: " + id);
	};
	
	zebraGame.updateHud = function() {
		if (!zebraGame.ShipIntegrityText) {
			zebraGame.ShipIntegrityText = new PIXI.Text("Ship is booting up...",{font:"30px Arial", fill:"red"});
			zebraGame.ShipIntegrityText.x = 800;
			zebraGame.ShipIntegrityText.y = 0;
			zebraGame.Hud.addChild(zebraGame.ShipIntegrityText);
		}
		
		if(!zebraGame.TimeLeftText) {
			zebraGame.TimeLeftText = new PIXI.Text("Time remaining: " + zebraGame.CurrentShip.RoundLengthInSeconds + "s",{font:"30px Arial", fill:"red"});
			zebraGame.TimeLeftText.x = 50;
			zebraGame.TimeLeftText.y = 0;
			zebraGame.Hud.addChild(zebraGame.TimeLeftText);
		}
		
		if (zebraGame.CurrentShip) {
			zebraGame.ShipIntegrityText.setText("Ship integrity: " + zebraGame.CurrentShip.Integrity + "%");
			zebraGame.TimeLeftText.setText("Time remaining: " + zebraGame.CurrentShip.RoundLengthInSeconds + "s");
		}		
	};
};
