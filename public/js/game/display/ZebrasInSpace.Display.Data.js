﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.data) == 'undefined') zebraCorp.zebrasinspace.display.data = {};

zebraCorp.zebrasinspace.display.data.extend = function (zebraGame) {
    debug('zebrasinspace.data initializing');

	zebraGame.startSocket = function() {
		zebraGame.socket = new WebSocket("ws://" + location.host + "/viewSocket");
		zebraGame.socket.onmessage = function(event) {
			zebraGame.parseMessage(event.data);
		}
	}
	
	zebraGame.sendMessage = function(playerId, command, param1, param2, param3) {
		//TODO we need a better check
		if (zebraGame.socket) {
			var message = '';
			
			if (playerId != null && typeof playerId !== "undefined" && !isNaN(playerId)) {
				message += playerId;
				
				if (command && command.length > 0) {
					message += ':' + command;
					
					if (param1 != null && typeof param1 !== "undefined" && (param1.length > 0 || !isNaN(param1))) {
						message += ':' + param1;
					
						if (param2 != null && typeof param2 !== "undefined" && (param2.length > 0 || !isNaN(param2))) {
							message += ':' + param2;
					
							if (param3 != null && typeof param3 !== "undefined" && (param3.length > 0 || !isNaN(param3))) {
								message += ':' + param3;
							}
						}
					}
				}
			}
			
			debug('sending message: ' + message)
			
			zebraGame.socket.send(message);
		}
	};

    zebraGame.parseMessage = function (message) {
		//id:command:parameter1:parameter2
		
    	//debug('zebrasinspace.data parsemessage ' + message);
    	
		if (message) {			
			var messageParts = message.split(':');
			var command, id, param1, param2, param3;
			
			if (messageParts.length > 0) {
				id = parseInt(messageParts[0],10);
								
				if (messageParts.length > 1) {
					command = messageParts[1];
				}
			}
			
			if(id === null || typeof id === 'undefined' || isNaN(id)) {
				id = -1;
			}
			
			if (command && command.length > 0) {
				switch(command) {
					case 'id': zebraGame.setGameId(id); break;
					case 'moveLeft': zebraGame.doPlayerMovementLeft(id); break;
					case 'moveRight': zebraGame.doPlayerMovementRight(id); break;
					case 'moveUp': zebraGame.doPlayerMovementUp(id); break;
					case 'moveDown': zebraGame.doPlayerMovementDown(id); break;
					case 'stopLeft': zebraGame.stopPlayerMovementLeft(id); break;
					case 'stopRight': zebraGame.stopPlayerMovementRight(id); break;
					case 'stopUp': zebraGame.stopPlayerMovementUp(id); break;
					case 'stopDown': zebraGame.stopPlayerMovementDown(id); break;
					case 'executeGoodAction': zebraGame.doPlayerGoodAction(id); break;
					case 'executeBadAction': zebraGame.doPlayerBadAction(id); break;
					case 'new-player': zebraGame.addPlayer(id); break;
					case 'username': zebraGame.setUsername(id, messageParts[2]); break;
				}
			}			
		}
    };
};