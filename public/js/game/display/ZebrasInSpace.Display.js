﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};

zebraCorp.zebrasinspace.display.initialize = function () {
    debug('zebrasinspace initializing');

    var zebraGame = {
        Stage: null,
		Renderer: null,
		gfxFolder: 'gfx/',
		Scene: null,
		
		UpdateIntervalId: null,
		UpdateIntervalDuration: 40,
		Ticks: 0,
		TicksPerSecond: 0,
		
		Players: [],
		GameObjects: [],
		CurrentShip: null,
		CurrentObjectId: 1000
    };

	zebraGame.TicksPerSecond = Math.round(1000/zebraGame.UpdateIntervalDuration);
   
    zebraCorp.zebrasinspace.display.controllers.extend(zebraGame);
    zebraCorp.zebrasinspace.display.helpers.extend(zebraGame);
    zebraCorp.zebrasinspace.display.events.extend(zebraGame);
    zebraCorp.zebrasinspace.display.player.extend(zebraGame);
    zebraCorp.zebrasinspace.display.data.extend(zebraGame);
    zebraCorp.zebrasinspace.display.gameobject.extend(zebraGame);    
	zebraCorp.zebrasinspace.display.background.extend(zebraGame);
	zebraCorp.zebrasinspace.display.hud.extend(zebraGame);
	zebraCorp.zebrasinspace.display.audio.extend(zebraGame);
	zebraCorp.zebrasinspace.display.ship.extend(zebraGame);
	zebraCorp.zebrasinspace.display.threats.extend(zebraGame);
	
	return zebraGame;
};