// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.player) == 'undefined') zebraCorp.zebrasinspace.display.player = {};

zebraCorp.zebrasinspace.display.player.extend = function (zebraGame) {
	
	zebraGame.createPlayer = function(playerId){
		
		debug('zebrasinspace.display.player.createPlayer with: ' + playerId);
		
		var player = {
				PlayerId: playerId,
                Username: "",
				Sprite: null,
				Texture: null,
				TextureLeft: null,
				TextureRight: null,
				TextureUp: null,
				TextureDown: null,				
				
				Removed: false,
				
				IsMovingLeft: false,
				IsMovingRight: false,
				IsMovingUp: false,
				IsMovingDown: false,
				IsDoingGoodAction: false,
				IsDoingBadAction: false,
				
				IsInfected: false,
				Health: 100,
				BatteryCharge: 100.0, //doing actions and moving uses battery
				BatteryMoveUsage: 0.05,
				BatteryActionUsage: 10.0,				
				Score: 0,
				
				MoveSpeed: 3.5,
				GrazeDistanceForInteraction: 5				
		};
		
		player.loadSprite = function(){			
			debug('zebrasinspace.display.player.loadSprite');
			
			var charColor;
			switch(player.PlayerId) {

		    case 0:
		    	charColor = "CharGreen";
		        break;
		    case 1:
		    	charColor = "CharYellow";
		        break;
		    case 2:
		    	charColor = "CharRed";
		        break;
		    case 3:
		    	charColor = "CharOrange";
		        break; 
		    case 4:
		    	charColor = "CharBlue";
		        break;    
		        
		    default:
		    	charColor = "CharGreen";
			} 
			
			player.Texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + charColor + "Left.png");
			player.TextureLeft = PIXI.Texture.fromImage(zebraGame.gfxFolder + charColor + "Left.png");
			player.TextureRight = PIXI.Texture.fromImage(zebraGame.gfxFolder + charColor + "Right.png");
			player.TextureUp = PIXI.Texture.fromImage(zebraGame.gfxFolder + charColor + "Up.png");
			player.TextureDown = PIXI.Texture.fromImage(zebraGame.gfxFolder + charColor + "Down.png");			
			player.Sprite = new PIXI.Sprite(player.TextureDown);			
						
			player.Sprite.height = 25;
			player.Sprite.width = 25;
			player.Sprite.anchor.x = 0.5;
			player.Sprite.anchor.y = 0.5;			

			player.Sprite.position.x = 30;
			player.Sprite.position.y = 30;	
		};	
		
		
		player.setPosition = function(point)
		{
			player.Sprite.position.x = point.x;
			player.Sprite.position.y = point.y;			
		};
		
		player.update = function(gameObjects) {
			if (player.Health <= 0) {
				player.Removed = true;
			}
			
			if (!player.Removed) {
				player.doMovement(gameObjects);
				player.doAction(gameObjects);
			}			
		};
		
		player.changeHealth = function(change) {
			player.Health += change;
			
			if (player.Health > 100) {
				player.Health = 100;
			} else if (player.Health < 0) {
				player.Health = 0;
			}
			
			zebraGame.sendMessage(player.PlayerId, 'health', player.Health);
		};
		
		player.changeBatteryCharge = function(change, sendMessage) {
			player.BatteryCharge += change;
			
			if (player.BatteryCharge > 100) {
				player.BatteryCharge = 100;
			} else if (player.BatteryCharge < 0) {
				player.BatteryCharge = 0;
			}
			
			if (sendMessage) {
				zebraGame.sendMessage(player.PlayerId, 'battery', player.BatteryCharge);
			} else {
				player.BatteryHasChanged = true;
			}			
		};
		
		player.changeScore = function(change) {
			player.Score += change;
			
			if (player.Score < 0) {
				player.Score = 0;
			}
						
			zebraGame.sendMessage(player.PlayerId, 'score', player.Score);
		};
		
		player.setInfection = function(infected) {
			player.IsInfected = infected;
			
			if(!infected) {
				zebraGame.ReduceThreatCounter();
			}
			
			zebraGame.sendMessage(player.PlayerId, 'infected', player.IsInfected);
		};
		
		player.sendAllCommands = function() {
			zebraGame.sendMessage(player.PlayerId, 'health', player.Health);
			zebraGame.sendMessage(player.PlayerId, 'battery', player.BatteryCharge);
			zebraGame.sendMessage(player.PlayerId, 'infected', player.IsInfected);
		};
		
		player.sendPendingCommands = function() {
			if (player.BatteryHasChanged) {
				player.BatteryHasChanged = false;
				zebraGame.sendMessage(player.PlayerId, 'battery', player.BatteryCharge);
			}
		};
		
		player.doMovement = function(gameObjects)
		{				
			var prevX = player.Sprite.position.x;
			var prevY = player.Sprite.position.y;
		
			var hasMoved = false;
			if (player.BatteryCharge > 0) {
				if (player.IsMovingLeft){
					hasMoved = true;				
					player.Sprite.position.x -= player.MoveSpeed;	
					player.Sprite.setTexture(player.TextureLeft);
					
					//player.Sprite.rotation = (1/360) * 180;
					//player.Sprite.rotation = (3.14 / 180) * 180;
				}			
				if(player.IsMovingRight){
					hasMoved = true;		
					player.Sprite.position.x += player.MoveSpeed;
					player.Sprite.setTexture(player.TextureRight);
				}
				if(player.IsMovingUp){
					hasMoved = true;		
					player.Sprite.position.y -= player.MoveSpeed;
					player.Sprite.setTexture(player.TextureUp);
				}
				if(player.IsMovingDown){
					hasMoved = true;		
					player.Sprite.position.y += player.MoveSpeed;
					player.Sprite.setTexture(player.TextureDown);
				}	
			}
			
			/*debug('is black: ' + zebraGame.isPositionBlack(player.Sprite.position.x, player.Sprite.position.y, player.Sprite.width / 2, player.Sprite.height / 2));
			debug('is colliding: ' + player.isCollidingWithOtherObjects(gameObjects));*/
			
			if (hasMoved && (zebraGame.isPositionBlack(player.Sprite.position.x, player.Sprite.position.y, player.Sprite.width / 2, player.Sprite.height / 2) || player.isCollidingWithOtherObjects(gameObjects))) {
				debug('reverting position as player has collided');
				player.Sprite.position.x = prevX;
				player.Sprite.position.y = prevY;
				hasMoved = false;
			}

			if(hasMoved) {
				player.changeBatteryCharge(-player.BatteryMoveUsage);
			}						
		};
		
		player.isCollidingWithOtherObjects = function(gameObjects) {
			var result = false;
			
			for (var i = 0, l = gameObjects.length; i < l; i++) {
				if (player.PlayerId !== gameObjects[i].PlayerId && player.isWithinDistanceOf(gameObjects[i], true)) {
					result = true;
					break;
				}
			}
			
			return result;
		};
		
		player.isWithinDistanceOf = function(gameObject, isCollisionDetect) {			
			var objectWidth = gameObject.Sprite.width;
			var playerWidth = player.Sprite.width;
			
			var distance = zebraGame.getDistance(
						gameObject.Sprite.position.x, 
						gameObject.Sprite.position.y,
						player.Sprite.position.x,
						player.Sprite.position.y);
			
			return distance <= (((objectWidth / 2) + (playerWidth / 2)) + (isCollisionDetect ? 0 : player.GrazeDistanceForInteraction));
		};
		
		player.applyGoodActionFromPlayer = function(playerObject) {
			debug('good action applied on object with id: ' + player.PlayerId + ' from playerId: ' + playerObject.PlayerId);
			
			var chargeToGive = 10;
			if (playerObject.BatteryCharge - chargeToGive < 0) {
				chargeToGive = playerObject.BatteryCharge / 2;
			}
			if (player.BatteryCharge + chargeToGive > 100) {
				chargeToGive = 100 - player.BatteryCharge;
			}
			
			player.changeBatteryCharge(chargeToGive);
			playerObject.changeBatteryCharge(-chargeToGive);		
			
			zebraGame.sendMessage(player.PlayerId, 'batteryReceived', chargeToGive);
			zebraGame.sendMessage(playerObject.PlayerId, 'batteryGiven', chargeToGive);
		};
		
		player.applyBadActionFromPlayer = function(playerObject) {
			debug('bad action applied on object with id: ' + player.PlayerId + ' from playerId: ' + playerObject.PlayerId);
			
			var chargeToTake = 10;
			if (player.BatteryCharge - chargeToTake < 0) {
				chargeToTake = player.BatteryCharge / 2;
			}
			if (playerObject.BatteryCharge + chargeToTake > 100) {
				chargeToTake = 100 - playerObject.BatteryCharge;
			}
			
			playerObject.changeBatteryCharge(chargeToTake);
			player.changeBatteryCharge(-chargeToTake);
			
			zebraGame.sendMessage(playerObject.PlayerId, 'batteryTaken', chargeToTake);
			zebraGame.sendMessage(player.PlayerId, 'batteryStolen', chargeToTake);
		};
		
		player.doAction = function(gameObjects)
		{
			if (player.IsDoingGoodAction || player.IsDoingBadAction) {
				for (var i = 0,l = gameObjects.length; i < l; i++) {
					if (player.PlayerId != gameObjects[i].PlayerId && player.isWithinDistanceOf(gameObjects[i])) {
						if (player.IsDoingGoodAction) {
							gameObjects[i].applyGoodActionFromPlayer(player);
						} else if (player.IsDoingBadAction) {
							gameObjects[i].applyBadActionFromPlayer(player);
						}
					}
				}	
			}			
			
			player.IsDoingGoodAction = false;
			player.IsDoingBadAction = false;
		};
		
		return player;		
	};
	
	
	
	
};