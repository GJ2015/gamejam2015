// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.ship) == 'undefined') zebraCorp.zebrasinspace.display.ship = {};

zebraCorp.zebrasinspace.display.ship.extend = function (zebraGame) {
	
	zebraGame.createShip = function(shipId){
		
		debug('zebrasinspace.display.player.createPlayer');
		
		var ship = {
				ShipId: shipId,
				Sprite: null,
				Destroyed: false,
				
				BackgroundUrl: "background01.png",
				LevelImageUrl: "levelImage01.png",
				DoorImageUrl: "doorImage01.png",
				
				StartPositions: [],
				ObjectPositions: [],
				PanelPositions: [],
				
				Integrity: 100,
				RoundLengthInSeconds: (5 * 60),
				GameStarted: false
		};
		
		//set player start positions
		ship.StartPositions.push({ x: 1400, y: 325});
		ship.StartPositions.push({ x: 1350, y: 575});
		ship.StartPositions.push({ x: 1600, y: 440});
		ship.StartPositions.push({ x: 990, y: 565});
		ship.StartPositions.push({ x: 400, y: 510});
		
		ship.ObjectPositions.push({ x: 365, y: 325});
		ship.ObjectPositions.push({ x: 425, y: 715});
		ship.ObjectPositions.push({ x: 1035, y: 525});
		ship.ObjectPositions.push({ x: 1615, y: 390});
		ship.ObjectPositions.push({ x: 430, y: 450});	
		ship.ObjectPositions.push({ x: 1400, y: 525});
		
		ship.PanelPositions.push({ x: 375, y: 515});	
		ship.PanelPositions.push({ x: 645, y: 570});
		ship.PanelPositions.push({ x: 385, y: 740});
		ship.PanelPositions.push({ x: 950, y: 565});
		ship.PanelPositions.push({ x: 1450, y: 305});
		ship.PanelPositions.push({ x: 1340, y: 630});
		ship.PanelPositions.push({ x: 1620, y: 440});		
		 
		ship.loadLevelSprite = function(){			
			debug('zebrasinspace.display.ship.loadSprite');
			
			var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + ship.LevelImageUrl);
			ship.LevelSprite = new PIXI.Sprite(texture);		
						
			zebraGame.Scene.addChild(ship.LevelSprite);
		};	
		
		ship.loadDoorSprite = function() {
			if (ship.DoorSprite) {
				zebraGame.Scene.removeChild(ship.DoorSprite);
			}
			
			var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + ship.DoorImageUrl);
			ship.DoorSprite = new PIXI.Sprite(texture);		
			
			zebraGame.Scene.addChild(ship.DoorSprite);
		};
		
		ship.loadWhiteSprite = function() {
			if (ship.BackgroundSprite) {
				zebraGame.Scene.removeChild(ship.DoorSprite);
			}
			
			var texture = PIXI.Texture.fromImage(zebraGame.gfxFolder + ship.BackgroundUrl);
			ship.BackgroundSprite = new PIXI.Sprite(texture);		
			
			zebraGame.Scene.addChild(ship.BackgroundSprite);			
		};
				
		ship.setPosition = function(x,y)
		{
			ship.Sprite.position.x = x;
			ship.Sprite.position.y = y;			
		};
		
		ship.update = function(gameObjects) {
			if (ship.Integrity <= 0) {
				//TODO ship is destroyed...
				ship.Destroyed = true;
                zebraGame.explodeShip();
                zebraGame.gameOver();
			}		
		};
		
		ship.addGameObjects = function () {			
			if (ship.ShipId === 1) {
				var objectPositionId = 0;
				
				var oilBarrel1 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "oildrum01");		
				oilBarrel1.loadSprite();
				oilBarrel1.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(oilBarrel1.Sprite);
				zebraGame.GameObjects.push(oilBarrel1);	
				 
				var oilBarrel2 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "oildrum01");		
				oilBarrel2.loadSprite();
				oilBarrel2.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(oilBarrel2.Sprite);
				zebraGame.GameObjects.push(oilBarrel2);
				
				var chargeStation01 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "chargstation01");		
				chargeStation01.loadSprite();
				chargeStation01.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(chargeStation01.Sprite);
				zebraGame.GameObjects.push(chargeStation01);
				
				var chargeStation02 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "chargstation01");		
				chargeStation02.loadSprite();
				chargeStation02.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(chargeStation02.Sprite);
				zebraGame.GameObjects.push(chargeStation02);
				
				var chargeStation03 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "chargstation01");		
				chargeStation03.loadSprite();
				chargeStation03.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(chargeStation03.Sprite);
				zebraGame.GameObjects.push(chargeStation03);
				
				var oilBarrel3 =  zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "oildrum01");		
				oilBarrel3.loadSprite();
				oilBarrel3.setPosition(ship.ObjectPositions[objectPositionId++]);
				zebraGame.Scene.addChild(oilBarrel3.Sprite);
				zebraGame.GameObjects.push(oilBarrel3);
			}
		};
		
		ship.getLeakingOilBarrel = function() {
			var result = null;
			for(var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
				if (zebraGame.GameObjects[i].ObjectType === 'leakingoildrum01') {
					result = zebraGame.GameObjects[i];
					break;
				}
			}
			return result;
		};
		
		ship.getOilBarrel = function() {
			var result = null;
			for(var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
				if (zebraGame.GameObjects[i].ObjectType === 'oildrum01' && zebraGame.GameObjects[i].ImpactToShipsHealth == 0) {
					result = zebraGame.GameObjects[i];
					break;
				}
			}
			return result;
		};
		
		ship.getPlayerWithNoInfection = function() {
			var result = null;
			
			if (zebraGame.Players.length > 1) {
				var allreadyHasInfection = false;
				for(var i = 0, l = zebraGame.Players.length; i < l; i++) {
					if (zebraGame.Players[i].IsInfected) {
						allreadyHasInfection = true;
						break;
					}
				}
				
				if (!allreadyHasInfection) {
					var playerIndex = zebraGame.RollDie(0, zebraGame.Players.length - 1);
					
					result = zebraGame.Players[playerIndex];
				}
			}		
			
			return result;
		};
		
		ship.getFaultyWire = function() {
			var result = null;
			for(var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
				if (zebraGame.GameObjects[i].ObjectType === 'faultywire01') {
					result = zebraGame.GameObjects[i];
					break;
				}
			}
			return result;
		};
		
		return ship;		
	};


	
	
};