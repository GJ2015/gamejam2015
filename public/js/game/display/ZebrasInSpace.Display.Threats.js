// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.threats) == 'undefined') zebraCorp.zebrasinspace.display.threats = {};

zebraCorp.zebrasinspace.display.threats.extend = function (zebraGame) {
    debug('zebrasinspace.display.threats extend');    
    
	zebraGame.NumberOfActiveThreaths = 0;
	
    zebraGame.updateThreats = function()
    {
		if (zebraGame.Players.length > 0 && zebraGame.NumberOfActiveThreaths <= zebraGame.Players.length + 1) {  //one more threath than amount of players
			var random = Math.random(); 
    	
			if (random <= 0.05) {								
				random = Math.random(); 
				debug('zebrasinspace.display.threats new random threat with ' + random);
				
				if (random < 0.3) {
					//add fire if any leaking	
					var objectOnFire = zebraGame.CurrentShip.getLeakingOilBarrel();
					
					if (!objectOnFire) {
						objectOnFire = zebraGame.CurrentShip.getFaultyWire();
					}
					
					if(objectOnFire) {							
						zebraGame.addFireAtPosition(objectOnFire.Sprite.position);

						zebraGame.NumberOfActiveThreaths++;						
					}
				} else if (random < 0.5) {
					//add infection if no player has it
					var player = zebraGame.CurrentShip.getPlayerWithNoInfection();
					
					if (player) {
						debug('OMG! INFECTION!');
						player.setInfection(true);		
						zebraGame.NumberOfActiveThreaths++;						
					}
				}
				else if (random < 0.7) {
					//add wire at free point		
					debug('OMG! FAULTY WIRES!');					
					var point = zebraGame.CurrentShip.PanelPositions[zebraGame.RollDie(1,7)];					
					
					zebraGame.addFaultyWireAtPosition(point);
										
					zebraGame.NumberOfActiveThreaths++;		
				} else if (random <= 1) {
					//add new leaking oil barrel
					
					
					var oilBarrel = zebraGame.CurrentShip.getOilBarrel();
					
					if (oilBarrel) {
						
						debug('OMG! OIL BARREL!');
						
						oilBarrel.ImpactToShipsHealth = 5;
						
						//switch image to leakingoildrum01.png
						var newTexture = PIXI.Texture.fromImage(zebraGame.gfxFolder + "leakingoildrum01.png");
						oilBarrel.Sprite.setTexture(newTexture);
						oilBarrel.VisibleIcon = true;
						zebraGame.Scene.addChild(oilBarrel.IconSprite);
						
						oilBarrel.ObjectType = 'leakingoildrum01';
						
						zebraGame.playAudio("oilspill_1");
						zebraGame.playAudio("oilleak");
						
						zebraGame.NumberOfActiveThreaths++;
					}
				} 	
			} 	
		}
    	    	
    };  
	
	zebraGame.addFireAtPosition = function(point) {
		var fire1 = zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "fire01", true);
		fire1.loadSprite();						
		fire1.setPosition(point);
		zebraGame.Scene.addChild(fire1.Sprite);
		zebraGame.Scene.addChild(fire1.IconSprite);
		fire1.VisibleIcon = true;
		zebraGame.GameObjects.push(fire1);
		
	};
	
	zebraGame.addFaultyWireAtPosition = function(point) {
		var faultywire01 = zebraGame.createGameObject(zebraGame.CurrentObjectId++, 100, 0, "faultywire01", true);
		faultywire01.loadSprite();						
		faultywire01.setPosition(point);
		zebraGame.Scene.addChild(faultywire01.Sprite);
		zebraGame.Scene.addChild(faultywire01.IconSprite);
		faultywire01.VisibleIcon = true;
		zebraGame.GameObjects.push(faultywire01);
	};
    
	zebraGame.ReduceThreatCounter = function() {		
		if (--zebraGame.NumberOfActiveThreaths < 0) {
			zebraGame.NumberOfActiveThreaths = 0;
		}
	};
 };