﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.helpers) == 'undefined') zebraCorp.zebrasinspace.display.helpers = {};

zebraCorp.zebrasinspace.display.helpers.extend = function (zebraGame) {
    debug('zebrasinspace.helpers initializing');

    zebraGame.emptyScreen = function() {
        /*zebraGame.Stage = new PIXI.Stage(0x66FF99);
        zebraGame.Root = new PIXI.DisplayObjectContainer();
        zebraGame.Stage.addChild(zebraGame.Root);
        // Update display
        requestAnimFrame(zebraGame.animate);*/
    };

    zebraGame.getPlayer = function (playerId) {
		//loop player array and return the object with the correct playerId		
		var player = null;
		for (var i = 0, l = zebraGame.Players.length; i < l; i++) {
			if (zebraGame.Players[i].PlayerId === playerId) {
				player = zebraGame.Players[i];
				break;
			}
		}
		
		return player;
    };
	
	zebraGame.getDistance = function(x1, y1, x2, y2) {
		var xs = 0;
		var ys = 0;
		 
		xs = x2 - x1;
		xs = xs * xs;
		 
		ys = y2 - y1;
		ys = ys * ys;
		 
		return Math.sqrt(xs + ys);
	};
	
	zebraGame.RollDie = function(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;	
	};
};