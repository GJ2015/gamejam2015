﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.events) == 'undefined') zebraCorp.zebrasinspace.display.events = {};

zebraCorp.zebrasinspace.display.events.extend = function (zebraGame) {
    debug('zebrasinspace.events initializing');
	
	zebraGame.initGame = function() {
		zebraGame.startSocket();
		zebraGame.initStage();		
		zebraGame.initHud();
		zebraGame.initShip();
		zebraGame.startUpdate();
		
		//debug: add default player to display when you want to play without a lein server
		//zebraGame.addPlayer(0);
	};
	
	zebraGame.initStage = function() {
		debug('zebrasinspace.events.InitStage');
		zebraGame.Stage = new PIXI.Stage(0xFFFFFF, true);	
		zebraGame.Root = new PIXI.DisplayObjectContainer();
		zebraGame.Scene = new PIXI.DisplayObjectContainer();
		zebraGame.Hud = new PIXI.DisplayObjectContainer();
		zebraGame.Stage.addChild(zebraGame.Root);
		
		zebraGame.Root.addChild(zebraGame.Scene);
		zebraGame.Root.addChild(zebraGame.Hud);
		/*var graphics = new PIXI.Graphics();
		graphics.beginFill(0xFFFF00);
		graphics.lineStyle(5, 0xFF0000);
		graphics.drawRect(0, 0, 1600, 900);
		zebraGame.Scene.addChild(graphics);*/
		
		zebraGame.Renderer = PIXI.autoDetectRenderer(400, 300);
		window.onresize = zebraGame.resizeWindow;
		zebraGame.resizeWindow();
		
		document.body.appendChild(zebraGame.Renderer.view);
		
		requestAnimFrame(zebraGame.animate);	
		
		zebraGame.Stage.click = function(mouseData){
			var localCoordsPosition = mouseData.getLocalPosition(zebraGame.CurrentShip.LevelSprite);
			   debug(localCoordsPosition);
			};
	};
	
	zebraGame.initShip = function() {		
		zebraGame.CurrentShip = zebraGame.createShip(1);
		
		zebraGame.initBackgroundCanvasForCurrentShip();
		
		zebraGame.CurrentShip.loadLevelSprite();
		//zebraGame.CurrentShip.loadWhiteSprite();
		
		zebraGame.CurrentShip.addGameObjects();		
	};
	
	zebraGame.addPlayer = function(playerId) {		
		var player = zebraGame.createPlayer(playerId);	
		player.loadSprite();
		player.setPosition(zebraGame.CurrentShip.StartPositions[playerId]);
		zebraGame.Scene.addChild(player.Sprite);		
		
		zebraGame.Players.push(player);		
		zebraGame.GameObjects.push(player);	
		
		//send all commands to remote
		player.sendAllCommands();
		
		zebraGame.CurrentShip.loadDoorSprite();
		
		zebraGame.CurrentShip.GameStarted = true;
	};
	
	zebraGame.animate = function() {	
	    requestAnimFrame(zebraGame.animate);
	
	    // render the stage   
	    zebraGame.Renderer.render(zebraGame.Stage);
	};
		
	zebraGame.resizeWindow = function() {	
		var x = 1600.0, y = 900.0;
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) - 20;
		var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 20;
	    var ratio;
	    if (w/h> x/y) {
			ratio = h/y
	    } else {
			ratio = w/x
	    }
		// resize renderer
		zebraGame.Renderer.resize(w, h);
		zebraGame.Root.scale.x = ratio;
		zebraGame.Root.scale.y = ratio;
	};

    zebraGame.setUsername = function(id, username) {
        var player = zebraGame.getPlayer(id);
        player.Username = String(username);
    };

	zebraGame.startUpdate = function() {
		debug('starting update interval with duration: ' + zebraGame.UpdateIntervalDuration);
		zebraGame.UpdateIntervalId = window.setInterval(function() {
			zebraGame.update();
		}, zebraGame.UpdateIntervalDuration);
	};
	
	zebraGame.update = function() {	
		zebraGame.Ticks += 1;
		
		//remove any Removed objects
		var tempGameObjects = [];
		for (var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
			if (zebraGame.GameObjects[i].Removed) {
				zebraGame.Scene.removeChild(zebraGame.GameObjects[i].Sprite);
				zebraGame.Scene.removeChild(zebraGame.GameObjects[i].IconSprite);
				if (zebraGame.GameObjects[i].IsDynamicThreath) {
					zebraGame.ReduceThreatCounter();
				}
				
				//zebraGame.stopLoopedAudio
				var anyOtherFires = false;
				for (var j = 0, l = zebraGame.GameObjects.length; j < l; j++) {
					if (j != i && zebraGame.GameObjects[j].ObjectType === 'fire01') {
						anyOtherFires = true;
					}
				}
				
				if(!anyOtherFires) {
					zebraGame.stopLoopedAudio(zebraGame.GameObjects[i].loopedAudio);
				}
			} else {
				tempGameObjects.push(zebraGame.GameObjects[i]);
			}
		}
		zebraGame.GameObjects = tempGameObjects;
	
		//do object updates (including players)
		for (var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
			zebraGame.GameObjects[i].update(zebraGame.GameObjects);
		}
		
		//some thing we want to only handle once a second
		if (zebraGame.Ticks % (zebraGame.TicksPerSecond) === 0) {						
			zebraGame.updateHud();
			
			var anyHasBattery = false;
			for(var i = 0, l = zebraGame.Players.length; i < l; i++) {				
				zebraGame.Players[i].sendPendingCommands();				
				if (zebraGame.Players[i].BatteryCharge > 0) {
					anyHasBattery = true;
				}
			}		
			
			//end game
			if (zebraGame.CurrentShip.Integrity <= 0 || zebraGame.CurrentShip.RoundLengthInSeconds <= 0 || (!anyHasBattery && zebraGame.CurrentShip.GameStarted)) {
				for(var i = 0, l = zebraGame.GameObjects.length; i < l; i++) {
					zebraGame.GameObjects[i].Removed = true;						
				}	

				zebraGame.gameOver();
			} else {
				if(zebraGame.CurrentShip.GameStarted) {
					zebraGame.CurrentShip.RoundLengthInSeconds--;
					if (zebraGame.CurrentShip.RoundLengthInSeconds < 0) {
						zebraGame.CurrentShip.RoundLengthInSeconds = 0;
					}
				}
			}		
		}
		
		zebraGame.updateThreats();
		zebraGame.updateAudio();		
	};	
};