// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.display) == 'undefined') zebraCorp.zebrasinspace.display = {};
if (typeof (zebraCorp.zebrasinspace.display.background) == 'undefined') zebraCorp.zebrasinspace.display.background = {};

zebraCorp.zebrasinspace.display.background.extend = function (zebraGame) {
    debug('zebrasinspace.display.background extend');    
    
    zebraGame.initBackgroundCanvasForCurrentShip = function(){ 		
		//create backgrund canvas for collision detect on player vs level
		zebraGame.BackgroundCanvasContext = zebraGame.getBackgroundCanvas();
    };
	
	zebraGame.getBackgroundCanvas = function() {		
		var myCanvas = document.createElement("canvas");
		myCanvas.width = 1920; 
		myCanvas.height = 1080;
		var myCanvasContext = myCanvas.getContext("2d");
		
		var imageObj = new Image();
		imageObj.onload = function() {
			debug('loading image');
			myCanvasContext.drawImage(imageObj, 0, 0, 1920, 1080);
		};
		imageObj.src = zebraGame.gfxFolder + zebraGame.CurrentShip.BackgroundUrl;		
		
		return myCanvasContext;
	};
	
	zebraGame.getTextureData = function(x, y, clipWidth, clipDepth, clipOffset) {			
		return zebraGame.BackgroundCanvasContext.getImageData(x + clipOffset, y + clipOffset, clipWidth, clipDepth);
	};

	zebraGame.isPositionBlack = function(x, y, width, height) {		
		var clipWidth = width;
		var clipDepth = height;
		var clipOffset = 1;
		var clipLength = clipWidth * clipDepth;
		
    	var colorData = zebraGame.getTextureData(x, y, clipWidth, clipDepth, clipOffset);   	
		/*
		for(var i = 0,l = colorData.data.length; i < l; i+= 4) {
			debug('#' + i + ' | ' + colorData.data[i] + ' | ' + colorData.data[i+1] + ' | ' + colorData.data[i+2]);
		}*/
		
		var isRed,isGreen,isBlue;
		
		// Loop through the clip and see if you find red, green, and blue. 
        for (var i = 0; i < clipLength * 4; i += 4) {
			isRed = true;
			isGreen = true;	
			isBlue = true;
			if (colorData.data[i] < 255) {
				//alert("red");
				isRed = false;
			} 
			if (colorData.data[i + 1] < 255) {
				//alert("green");
				isGreen = false;
			} 
			if (colorData.data[i + 2] < 255) {
				//alert("blue");
				isBlue = false;
			}
			// Fourth element is alpha and we don't care. 
			if(!isRed && !isGreen && !isBlue) {
				break;
			}
        }
		
    	return !isRed && !isGreen && !isBlue;
	};
    
    zebraGame.isPositionWhite = function(x, y){
		var clipWidth = 20;
		var clipDepth = 20;
		var clipOffset = 5;
		var clipLength = clipWidth * clipDepth;
		
    	var colorData = zebraGame.getTextureData(x, y, clipWidth, clipDepth, clipOffset);   	
		
		var isRed = false;
		var isGreen = false;	
		var isBlue = false;
		
		// Loop through the clip and see if you find red, green, and blue. 
        for (var i = 0; i < clipLength * 4; i += 4) {
			isRed = false;
			isGreen = false;	
			isBlue = false;
			if (colorData.data[i] == 255) {
				//alert("red");
				isRed = true;
			} 
			if (colorData.data[i + 1] == 255) {
				//alert("green");
				isGreen = true;
			} 
			if (colorData.data[i + 2] == 255) {
				//alert("blue");
				isBlue = true;
			}
			// Fourth element is alpha and we don't care. 
			if(isRed && isGreen && isBlue) {
				break;
			}
        }
		
    	return isRed && isGreen && isBlue;    	
    };
};