﻿var isDebug = false;
var urlParams = {};

var ZebraGameDisplay = function () {	
    var self = this;
    
    self.ZebrasInSpaceDisplay = null;
    
    self.start = function () {
		self.InitUrlParameters();
		
		isDebug = Boolean(self.GetUrlParameter('isDebug'));
		
        self.ZebrasInSpaceDisplay = zebraCorp.zebrasinspace.display.initialize();
        
        self.setupControllers();
		
		self.ZebrasInSpaceDisplay.initGame();
    };

    self.setupControllers = function () {
        document.onkeydown = function (e) {
            e = e || window.event;
			if (e.which !== 122)  //f11
				e.preventDefault();
            self.ZebrasInSpaceDisplay.executeKeyDown(e.which);
        };

        document.onkeyup = function (e) {
            e = e || window.event;
			e.preventDefault();
            self.ZebrasInSpaceDisplay.executeKeyUp(e.which);
        };
    };
	
	self.InitUrlParameters = function () {
        //set url parameters to global object
        (function () {
            var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.search.substring(1);
            while (e = r.exec(q))
                urlParams[d(e[1]).toLowerCase()] = d(e[2]);
        })();
    };
	
	self.GetUrlParameter = function (key) {
        return urlParams[key.toLowerCase()];
    };
};

var zebraGameDisplay = new ZebraGameDisplay();

var startZebraDisplay = function () { 
    zebraGameDisplay.start();
};