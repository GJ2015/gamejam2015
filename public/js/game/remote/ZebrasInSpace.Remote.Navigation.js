// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.navigation) == 'undefined') zebraCorp.zebrasinspace.remote.navigation = {};

zebraCorp.zebrasinspace.remote.navigation.extend = function(zebraGame) {
    
	zebraGame.initNavigation = function() {
		
		function makeButton(x, y, pic, start, stop) {
			var robot = new PIXI.Texture.fromImage("gfx/" + pic);
			var up = new PIXI.Sprite(robot);
			up.position.x = x;
			up.position.y = y;
			up.height = 100;
			up.width = 100;			

			up.anchor.x = 0.5;
			up.anchor.y = 0.5;	
			up.interactive = true;
			up.mousedown = up.touchstart = function(mousedata) {
				zebraGame.socket.send(start);
			}

			up.mouseup = up.touchend = function(mousedata) {
				if (stop != null)
					zebraGame.socket.send(stop);
			}

			up.touchendoutside = up.mouseup;

			zebraGame.Hud.addChild(up);
		}

		makeButton(300, 1000, "rightArrow.png", "moveRight", "stopRight");
		makeButton(100, 1000, "leftArrow.png","moveLeft",  "stopLeft");
		makeButton(200, 900,  "arrow.png","moveUp",    "stopUp");
		makeButton(200, 1100, "downArrow.png","moveDown",  "stopDown");


		makeButton(550, 1000,"good.png", "executeGoodAction",  null);
		makeButton(650, 1000,"bad.png", "executeBadAction",  null);

	}

};