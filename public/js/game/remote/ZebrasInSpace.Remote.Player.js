// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.player) == 'undefined') zebraCorp.zebrasinspace.remote.player = {};

zebraCorp.zebrasinspace.remote.player.extend = function (zebraGame) {

    zebraGame.Player = {
        UserID: 0,
        IsInfected: false,
        Health: 100,
        BatteryCharge: 100,
        Score: 0,
        Name: ""
    };

    zebraGame.setRemotePlayerInfected = function(isInfected) {
        zebraGame.Player.IsInfected = Boolean(isInfected);
        var image = "gfx/remoteDroid";
        if (zebraGame.Player.IsInfected) {
            image += "Bad";
        }
        else {
            image += "Good"
        }
        zebraGame.PlayerRobotIcon = new PIXI.Sprite(new PIXI.Texture.fromImage(image + ".png"));
    };

    zebraGame.updateRemoteLoginInformation = function(id) {
        zebraGame.Player.UserID = Number(id);
        zebraGame.socket.send("username:" + zebraGame.Player.Name)
    };

    zebraGame.updateRemotePlayerName = function(name) {
        zebraGame.Player.Name = String(name);
        zebraGame.PlayerNameText.setText("Name: " + zebraGame.Player.Name);
    };

    zebraGame.updateRemotePlayerHealth = function(health) {
    	
    	var prevHealth = zebraGame.Player.Health;
    	
        zebraGame.Player.Health = Number(health);
        zebraGame.PlayerHealthText.setText("Health: " + zebraGame.Player.Health + "%");
        
        if (prevHealth > zebraGame.Player.Health){
        	zebraGame.playAudio("stun_1");
        }
    };

    zebraGame.updatePlayerBatteryCharge = function(charge) {
        //zebraGame.Player.BatteryCharge = Number(charge).toFixed(1);
    	
    	var prevBatteryCharge = zebraGame.Player.BatteryCharge;    	
		zebraGame.Player.BatteryCharge = parseInt(charge,10);		
        zebraGame.PlayerBatteryLevelText.setText("Power: " + zebraGame.Player.BatteryCharge + "%");
        
        if(zebraGame.Player.BatteryCharge > prevBatteryCharge){
        	zebraGame.playAudio("charge_1");
        }
        
    };

    zebraGame.updatePlayerScore = function(score) {
        zebraGame.Player.Score = Number(score);
        zebraGame.PlayerScoreText.setText("Score: " + zebraGame.Player.Score);
    };

};