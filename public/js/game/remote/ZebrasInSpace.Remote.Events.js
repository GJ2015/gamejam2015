﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.events) == 'undefined') zebraCorp.zebrasinspace.remote.events = {};

zebraCorp.zebrasinspace.remote.events.extend = function (zebraGame) {
    debug('zebrasinspace.events initializing');
	
	zebraGame.initRemote = function() {
		zebraGame.startSocket();
	};

    zebraGame.emptyScreen = function() {
        debug("zebrasinspace.events.emptyScreen")
        zebraGame.Stage = new PIXI.Stage(0x66FF99, false);
        zebraGame.Root = new PIXI.DisplayObjectContainer();
        zebraGame.Stage.addChild(zebraGame.Root);
        requestAnimFrame(zebraGame.animate);
    };

    zebraGame.initScreen = function() {
        debug('zebrasinspace.events.initScreen');
        var interactive = true;
        zebraGame.Stage = new PIXI.Stage(0x66FF99, interactive);
        zebraGame.Root = new PIXI.DisplayObjectContainer();
        zebraGame.Hud = new PIXI.DisplayObjectContainer();
        zebraGame.Navigation = new PIXI.DisplayObjectContainer();

        zebraGame.Stage.addChild(zebraGame.Root);
        zebraGame.Root.addChild(zebraGame.Navigation);
        zebraGame.Root.addChild(zebraGame.Hud);
        var graphics = new PIXI.Graphics();
        graphics.beginFill(0xFFFF00);
        graphics.lineStyle(5, 0xFF0000);
        graphics.drawRect(0, 0, 800, 1200);
        zebraGame.Navigation.addChild(graphics);
        zebraGame.Renderer = PIXI.autoDetectRenderer(400, 300);
        document.body.appendChild(zebraGame.Renderer.view);
        window.onresize = zebraGame.resizeWindow;
        zebraGame.resizeWindow();
        //zebraGame.startUpdate();

        // Init HUD
        zebraGame.initHud();
        zebraGame.initNavigation();

        requestAnimFrame(zebraGame.animate);
    };

	zebraGame.joinGame = function() {
	var id = document.getElementById("login");
		zebraGame.Player.Name = String(document.getElementById("username").value);
        zebraGame.initScreen();
        zebraGame.setupControllers();
		zebraGame.socket.send("id:" + id.value);
		debug("Joining game ID: " + id.value);
        // Hide login form
        document.getElementById("join-game").style.display = 'none';
	};

    zebraGame.animate = function() {
        requestAnimFrame(zebraGame.animate);

        // render the stage
        zebraGame.Renderer.render(zebraGame.Stage);
    };

    zebraGame.resizeWindow = function() {
        var x = 800.0, y = 1200.0;
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) - 20;
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 20;
        var ratio;
        if (w/h> x/y) {
            ratio = h/y;
            zebraGame.Renderer.resize(x*ratio, h);
        } else {
            ratio = w/x;
            zebraGame.Renderer.resize(w, h);
        }
        zebraGame.Root.scale.x = ratio;
        zebraGame.Root.scale.y = ratio;
    };

    zebraGame.gameOver = function() {
        // Empty screen
        zebraGame.emptyScreen();

        //game over screen
        var texture = PIXI.Texture.fromImage("gfx/EndScreen.png");
        var sprite = new PIXI.Sprite(texture);
        zebraGame.Root.addChild(sprite);
        var GameOverText = new PIXI.Text("GAME OVER", {font: "30px Arial", fill: "red"});
        GameOverText.x = 50;
        GameOverText.y = 50;
        zebraGame.Root.addChild(GameOverText);
        var PlayerScoreText = new PIXI.Text("Your score: " + zebraGame.Player.Score, {font: "20px Arial", fill: "blue"});
        PlayerScoreText.x = 50;
        PlayerScoreText.y = 80;
        zebraGame.Root.addChild(PlayerScoreText);
        requestAnimFrame(zebraGame.animate);
    };

};