// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.hud) == 'undefined') zebraCorp.zebrasinspace.remote.hud = {};

zebraCorp.zebrasinspace.remote.hud.extend = function(zebraGame) {

    zebraGame.initHud = function() {
        // Player name
        zebraGame.PlayerNameText = new PIXI.Text("Name: " + zebraGame.Player.Name,
            {font: "30px Arial", fill: "black"});
        zebraGame.PlayerNameText.x = 20;
        zebraGame.PlayerNameText.y = 20;
        // Player health
        zebraGame.PlayerHealthText = new PIXI.Text("Health: " + zebraGame.Player.Health + "%",
            {font: "30px Arial", fill: "red"});
        zebraGame.PlayerHealthText.x = 20;
        zebraGame.PlayerHealthText.y = 50;
        // Player battery level
        zebraGame.PlayerBatteryLevelText = new PIXI.Text("Power: " + zebraGame.Player.BatteryCharge + "%",
            {font: "30px Arial", fill: "blue"}
        );
        zebraGame.PlayerBatteryLevelText.x = 20;
        zebraGame.PlayerBatteryLevelText.y = 80;
        // Player score
        zebraGame.PlayerScoreText = new PIXI.Text("Score: " + zebraGame.Player.Score,
            {font: "30px Arial", fill: "green"}
        );
        zebraGame.PlayerScoreText.x = 20;
        zebraGame.PlayerScoreText.y = 110;

        // Default robot icon
        zebraGame.PlayerRobotIcon = new PIXI.Sprite(new PIXI.Texture.fromImage("gfx/remoteDroidGood.png"));
        zebraGame.PlayerRobotIcon.x = 100;
        zebraGame.PlayerRobotIcon.y = 200;

        zebraGame.Hud.addChild(zebraGame.PlayerNameText);
        zebraGame.Hud.addChild(zebraGame.PlayerHealthText);
        zebraGame.Hud.addChild(zebraGame.PlayerBatteryLevelText);
        zebraGame.Hud.addChild(zebraGame.PlayerScoreText);
        zebraGame.Hud.addChild(zebraGame.PlayerRobotIcon);
    };
};