﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.controllers) == 'undefined') zebraCorp.zebrasinspace.remote.controllers = {};

zebraCorp.zebrasinspace.remote.controllers.extend = function (zebraGame) {
    debug('zebrasinspace.controllers initializing');
	/*
	backspace 	8
	tab 	9
	enter 	13
	shift 	16
	ctrl 	17
	alt 	18
	pause/break 	19
	caps lock 	20
	escape 	27
	page up 	33
	page down 	34
	end 	35
	home 	36
	left arrow 	37
	up arrow 	38
	right arrow 	39
	down arrow 	40
	insert 	45
	delete 	46
	0 	48
	1 	49
	2 	50
	3 	51
	4 	52
	5 	53
	6 	54
	7 	55
	8 	56
	9 	57 */
	var keysPressed = [];
    zebraGame.executeKeyDown = function (keyId) {
		if (!keysPressed[keyId]) {
			keysPressed[keyId] = true;
			
			switch (keyId) {
				//left arrow
				case 37:
					debug('left arrow down')
					zebraGame.socket.send("moveLeft")
					break;
				//up arrow
				case 38:
					debug('up arrow down')
					zebraGame.socket.send("moveUp")
					break;
				//right arrow
				case 39:
					debug('right arrow down')
					zebraGame.socket.send("moveRight")
					break;
				//down arrow
				case 40:
					debug('down arrow down')
					zebraGame.socket.send("moveDown")
					break;
				// q
				case 81:
					debug('good action down')
					zebraGame.socket.send("executeGoodAction")
					break;
				// w
				case 87:
					debug('bad action down')
					zebraGame.socket.send("executeBadAction")
					break;
			}
		}       
    };

    zebraGame.executeKeyUp = function (keyId) {
		keysPressed[keyId] = false;
		
        switch (keyId) {
			//left arrow
            case 37:
				debug('left arrow up')
				zebraGame.socket.send("stopLeft")
				break;
			//up arrow
			case 38:
				debug('up arrow up')
				zebraGame.socket.send("stopUp")
				break;
			//right arrow
			case 39:
				debug('right arrow up')
				zebraGame.socket.send("stopRight")
				break;
			//down arrow
			case 40:
				debug('down arrow up')
				zebraGame.socket.send("stopDown")
				break;
        }
    };

    zebraGame.setupControllers = function () {
        document.onkeydown = function (e) {
            e = e || window.event;
			e.preventDefault();
			zebraGame.executeKeyDown(e.which);
        };

        document.onkeyup = function (e) {
            e = e || window.event;
			e.preventDefault();
			zebraGame.executeKeyUp(e.which);
        };
    };

};