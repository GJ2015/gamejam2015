﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.helpers) == 'undefined') zebraCorp.zebrasinspace.remote.helpers = {};

zebraCorp.zebrasinspace.remote.helpers.extend = function (zebraGame) {
    debug('zebrasinspace.helpers initializing');

    zebraGame.someFunction = function () {

    };
};