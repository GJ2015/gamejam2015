// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.data) == 'undefined') zebraCorp.zebrasinspace.remote.data = {};

zebraCorp.zebrasinspace.remote.data.extend = function (zebraGame) {
    debug('zebrasinspace.data initializing');

	zebraGame.startSocket = function() {
		zebraGame.socket = new WebSocket("ws://" + location.host + "/controlSocket");
		zebraGame.socket.onmessage = function(event) {
			zebraGame.parseMessage(event.data);
		}
	};

    zebraGame.parseMessage = function (message) {
		//id:command:parameter1:parameter2
		
    	debug('zebrasinspace.data parsemessage ' + message);
    	
		if (message) {			
			var messageParts = message.split(':');
			var command = messageParts[0];

			if (command && command.length > 0) {
				switch(command) {
                    // Sent from server when user is logged in
                    case "start":
                        zebraGame.updateRemoteLoginInformation(messageParts[1]);
                        break;
                    case "health":
                        zebraGame.updateRemotePlayerHealth(messageParts[1]);
                        break;
                    case "battery":
                        zebraGame.updatePlayerBatteryCharge(messageParts[1]);
                        break;
                    case "score":
                        zebraGame.updatePlayerScore(messageParts[1]);
                        break;
                    case "infected":
                        zebraGame.setRemotePlayerInfected(messageParts[1]);
                        break;
                    case "gameover":
                        zebraGame.gameOver();
                        break;
                    default:
                        break;
				}
			}
			
		}
    };
};