// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};
if (typeof (zebraCorp.zebrasinspace.remote.audio) == 'undefined') zebraCorp.zebrasinspace.remote.audio = {};

zebraCorp.zebrasinspace.remote.audio.extend = function (zebraGame) {
    debug('zebrasinspace.display.audio extend');
    
    zebraGame.playAudio = function(audio){
    	document.getElementById(audio).play();
    }
    
    zebraGame.playLoopedAudio = function(audio){
    	document.getElementById(audio).loop = true;
    	document.getElementById(audio).play();
    }
    
    zebraGame.stopLoopedAudio = function(audio){
    	document.getElementById(audio).loop = false;
    	document.getElementById(audio).pause();
    }
   
    // sequences is an array
    zebraGame.Sequences = [];
    
    
    zebraGame.playAudioSequence = function(){   	
    	if (arguments.length > 0) {
    		
    		var sequence = [].slice.call(arguments);    		
    		zebraGame.addAudioSequence(sequence);
    		document.getElementById(arguments[0]).play();
    	}
    }   
    
    zebraGame.addAudioSequence = function(sequence){    	
    	
    	zebraGame.Sequences.push(sequence);
    }
    
    zebraGame.removeAudioSequence = function(sequence){
    	debug('zebrasinspace.display.audio audio removing ' + sequence);
    	var pos = zebraGame.Sequences.indexOf(sequence);
    	zebraGame.Sequences.splice(pos, 1);
    }
    
    
    zebraGame.updateAudio = function() {
    	// loop over audio
    	for (var i = zebraGame.Sequences.length -1; i >= 0 ; i--) {
    	    debug('zebrasinspace.display.audio updateAudio ' + zebraGame.Sequences[i]);
    	    zebraGame.updateSequence(zebraGame.Sequences[i]);
    	}
    }
    
    zebraGame.updateSequence = function(sequence){
    	
    	if (document.getElementById(sequence[0]).ended){
    		debug('zebrasinspace.display.audio audio ended ' + sequence);
    		sequence.shift();
    		if (sequence.length > 0) {
    			document.getElementById(sequence[0]).play();
    		}
    		else {
    			zebraGame.removeAudioSequence(sequence);
    		}
    	}

    }
    
};