﻿// Lazy initialize our namespace context: sgs.model.savingsgoal
if (typeof (zebraCorp) == 'undefined') zebraCorp = {};
if (typeof (zebraCorp.zebrasinspace) == 'undefined') zebraCorp.zebrasinspace = {};
if (typeof (zebraCorp.zebrasinspace.remote) == 'undefined') zebraCorp.zebrasinspace.remote = {};

zebraCorp.zebrasinspace.remote.initialize = function () {
    debug('zebrasinspace initializing');

    var zebraGame = {
        Stage: null,
		Renderer: null
    };

    zebraCorp.zebrasinspace.remote.controllers.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.helpers.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.events.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.data.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.player.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.hud.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.navigation.extend(zebraGame);
    zebraCorp.zebrasinspace.remote.audio.extend(zebraGame);

	return zebraGame;
};