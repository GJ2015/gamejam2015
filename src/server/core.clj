(ns server.core
 (:use [ring.middleware.reload :as reload]
       [compojure.route :only [files not-found]]
       [compojure.handler :only [site]] ; form, query params decode; cookie; session, etc
       [compojure.core :only [defroutes GET POST DELETE ANY context]]
       org.httpkit.server)
 (:gen-class))

       
(def games (agent {}))  ; {game-id { :channel, :clients }
(def clients (agent {})) ; {client-channel { :game-id, :client-id } }
     
                    
(defn end-game [id]
    (send games 
        (fn [games-map]
            (println "Ended game" id)
            (dissoc games-map id))))
     
(defn send-to-client [game-id client-id data]
	(let [chan (nth (get-in (deref games) [game-id :clients]) client-id)]
		(send! chan data)))

(defn new-game [channel]
    (fn [games-map]
        (loop [id (rand-int 99)]
            (if (contains? games-map id)         ; check if key exists
                (recur (rand-int 99))            ; if so, try another key
                (do (send! channel (str id ":id")); else  send back id and put it in
                    (on-close channel (fn [status] (end-game id)))
                    (on-receive channel (fn [data]
                          (let [colon (.indexOf data ":")
                                client-id (Integer. (subs data 0 colon))
                                msg (subs data (inc colon))]
                           (send-to-client id client-id msg))))
                    (assoc games-map id {:channel channel, 
                                         :clients []}))))))
    
(defn viewSockethandler [request]
  (with-channel request channel
    (println "channel opened")
    (send games (new-game channel))))
                          
(defn send-to-game [client-ch data]
    (if-let [client    (get (deref clients) client-ch)]
		(let  [game-id (client :game-id)
			   client-id (client :client-id) 
		       game-channel (get-in (deref games) [game-id :channel])]
		    (send! game-channel (str client-id ":" data)))
		(println "no such client")))
            

                  
(defn new-client [channel id]
    (println "new-client" id)
    (if (contains? (deref games) id)
        (send games 
                  (fn [games-map] 
                    (let [new-client-id (count (get-in games-map [id :clients]))]
                        (println (str "making new player: " new-client-id))
                        (send! (get-in games-map [id :channel]) (str new-client-id ":new-player"))
                        (send! channel (str "start:" new-client-id))
						            (send clients #(assoc % channel {:game-id id, :client-id new-client-id}))
						            (update-in games-map [id :clients] #(conj % channel)))))
		(send! channel (str "error:no game with id " id))))
                          

(defn end-client [channel]
	(send clients 
		(fn [old]
			(if-let [id (get-in old [channel :client-id])]
				(do
					(println "Ending client: " id)
					(dissoc old channel))
				(println (str "no such client" (deref clients)))))))
					
(defn controlSockethandler [request]
  (with-channel request channel
    (println "channel opened")
    (on-close   channel 
                (fn [status] (println "should close: " channel)))
    (on-receive channel
                (fn [data]
                    (println data)
                    (if (.startsWith data "id:")
                        (new-client channel (Integer. (subs data 3)))
                        (send-to-game channel data))))))
       
(defroutes all-routes
  (GET "/gull" [] "What do you call the king of gulls? Regull!")
  (GET "/viewSocket" [] viewSockethandler)           ;; websocket
  (GET "/controlSocket" [] controlSockethandler)     ;; websocket
  (files "/")
  (not-found "<p>Page not found.</p>")) ;; all other, return 404
   
(defn -main [& args]
    (run-server (reload/wrap-reload (site #'all-routes)) {:port 8080}))
    